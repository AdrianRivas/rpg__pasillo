﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battler : MonoBehaviour
{
    [SerializeField] private string _name = "", _gender = "", _class = "", _extraInfo = "";
    [SerializeField] private float _tempAtk, _tempMAtk, _tempDef, _tempSpeed;
    [SerializeField] private Sprite _sprite;
    [SerializeField] private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }    
}
