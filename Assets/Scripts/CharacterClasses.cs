﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterClasses : MonoBehaviour
{
    [SerializeField] private string _class = "", _extraInfo = "";
    [SerializeField] private float _life, _mana, _atk, _mAtk, _def, _speed;
    [SerializeField] private GameObject[] _bag, _equipment;
    [SerializeField] private Sprite _sprite;
    [SerializeField] private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
