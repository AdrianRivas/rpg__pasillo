﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehavior : MonoBehaviour
{
    [SerializeField] private string _name = "", _gender = "", _class = "", _extraInfo = "";
    [SerializeField] private float _life, _mana, _atk, _mAtk, _def, _speed;
    [SerializeField] private GameObject[] _bag, _equipment,_movementPoints;
    [SerializeField] private Sprite _sprite;
    [SerializeField] private Animator _animator;
    [SerializeField] private bool _isMoving, _isBattle, _isMenu,_toPiontB;
    int b = -1;
    static GameObject player;
    private void Awake()
    {
        player = GameObject.Find("PlayerData");
        DontDestroyOnLoad(player);
    }
    // Start is called before the first frame update
    void Start()
    {
        _movementPoints = null;
    }

    // Update is called once per frame
    void Update()
    {
        if ( _movementPoints==null)
        {
            try
            {
                Debug.Log("Finding");
                _movementPoints = GameObject.Find("PointsB").GetComponent<PointsB_DataBase>().pointsBData;
                transform.position = GameObject.Find("Main Camera").GetComponent<Transform>().transform.position;
                GameObject.Find("Main Camera").GetComponent<Transform>().parent=transform;
                StartCoroutine(MoveToB());
                Debug.Log("init co");
            }
            catch (System.Exception)
            {

                //throw;
            }
        }
        if (Input.GetKeyDown(KeyCode.Tab)==true)
        {
            OpenMenu()
                ;
        }
    }
    public void OpenMenu()
    {
        if (_isMenu==true)
        {
            _isMenu = false;
            //close ui Menu
        }
        else
        {
            _isMenu = true;
            //open ui menu
        }
    }
    public void Battle()
    {

    }
    IEnumerator MoveToB()
    {
        while (_toPiontB==true)
        {
            if (_isMoving==true)
            {
                if (transform.position == _movementPoints[b+1].transform.position)
                {
                    b++;
                }
                else if(_isMenu == false  || _isBattle == false)
                {
                    transform.position = Vector3.MoveTowards(transform.position, _movementPoints[b+1].transform.position, 1 * Time.deltaTime);
                }
            }
            yield return new WaitForSeconds(0.0001f);
        }

    }
}
