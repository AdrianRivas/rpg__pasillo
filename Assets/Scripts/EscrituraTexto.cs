﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EscrituraTexto : MonoBehaviour
{
    public string textoEscribir;
    public Text texto;
    public AudioSource audio;
    public AudioSource audio2;
    public AudioClip sound;
    public TMP_Text textoTMP;
    // Start is called before the first frame update
    void Start()
    {
        audio.clip = sound;
        textoEscribir = textoTMP.text;
        textoTMP.text = "";
        if (texto!=null)
        {
            
            StartCoroutine(EscribirRoutine());
        }
        if (textoTMP!=null)
        {
            StartCoroutine(TMPEscribirRoutine());
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator TMPEscribirRoutine(){
        for (int i = 0; i < textoEscribir.Length; i++)
        {

            textoTMP.text += textoEscribir[i];
            if (textoEscribir[i].ToString() == " ")
            {
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                audio.Play();
            }
            yield return new WaitForSeconds(0.02f);
        }
    }
    
    public IEnumerator EscribirRoutine()
    {
        textoTMP.text = "";
        int x = 0;
        while (textoEscribir[x] != '\0')
        {
            
            if (textoEscribir[x].Equals("") == false || textoEscribir[x].Equals(" ") == false)
            {
                texto.text += textoEscribir[x];
                audio.Play();
                yield return new WaitForSeconds(0.08f);
            }
            else
            {
                texto.text += textoEscribir[x];
            }
            x++;
        }
    }
}
